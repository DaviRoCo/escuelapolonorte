#include "Curso.h"

Curso::Curso()
{

}

Curso::Curso(int codigo, string nombre, int creditos, int horas)
{
	this->Codigo = codigo;
	this->Nombre = nombre;
	this->Creditos = creditos;
	this->Horas = horas;
}

int Curso::getCodigo()
{
	return this->Codigo;
}

string Curso::getNombre()
{
	return this->Nombre;
}

int Curso::getCreditos()
{
	return this->Creditos;
}

int Curso::getHoras()
{
	return this->Horas;
}

void Curso::setCodigo(int codigo)
{
	this->Codigo = codigo;
}

void Curso::setNombre(string nombre)
{ 
	this->Nombre = nombre;
}

void Curso::setCreditos(int creditos)
{
	this->Creditos = creditos;
}

void Curso::setHoras(int horas)
{
	this->Horas = horas;
}
