#pragma once
#include <string>
using namespace std;
class Curso
{
private:
	int Codigo;
	string Nombre;
	int Creditos;
	int Horas;
public:
	Curso();
	Curso(int, string, int, int);
	int getCodigo();
	string getNombre();
	int getCreditos();
	int getHoras();
	void setCodigo(int);
	void setNombre(string);
	void setCreditos(int);
	void setHoras(int);
};

