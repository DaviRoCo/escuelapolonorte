#include "Duende.h"

Duende::Duende()
{
}

Duende::Duende(int id, string nombre, int edad, bool becado)
{
    this->ID = id;
    this->Nombre = nombre;
    this->Edad = edad;
    this->Becado = becado;
}

int Duende::getID()
{
    return this->ID;
}

string Duende::getNombre()
{
    return this->Nombre;
}

int Duende::getEdad()
{
    return this->Edad;
}

bool Duende::getBeca()
{
    return this->Becado;
}

void Duende::setID(int id)
{
    this->ID = id;
}

void Duende::setNombre(string nombre)
{
    this->Nombre = nombre;
}

void Duende::setEdad(int edad)
{
    this->Edad = edad;
}

void Duende::setBeca(bool becado)
{
    this->Becado = becado;
}
