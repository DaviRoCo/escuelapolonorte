#pragma once
#include <string>
using namespace std;
class Duende
{
private:
	int ID;
	string Nombre;
	int Edad;
	bool Becado;
public:
	Duende();
	Duende(int, string, int, bool);
	int getID();
	string getNombre();
	int getEdad();
	bool getBeca();

	void setID(int);
	void setNombre(string);
	void setEdad(int);
	void setBeca(bool);
};

