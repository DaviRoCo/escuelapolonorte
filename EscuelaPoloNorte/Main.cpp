#include <iostream>
<<<<<<< HEAD
#include <limits>

=======
>>>>>>> David
using namespace std;
#include "listaCursos.h"
#include "listaDuendes.h"
#include "listaMatriculas.h"
listaCursos* LC = new listaCursos();
listaDuendes* LD = new listaDuendes();
listaMatriculas* LM = new listaMatriculas();
// metodo para capturar un numero, devuelve un entero
int capturarDato() {
    bool flag = false;
    int dato;
    while (!flag)
    {
        //cout << "Ingrese un numero: " << endl;
        cin >> dato;

        if (!cin)
        {
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            cout << "valor invalido." << endl;
        }
        else {
            flag = true;
        }
    }

    return dato;
}
// metodo para interfaz grafica
void separator() {
    cout << "\n<==============================================>" << endl;
}

int seleccionarOpcion() {
    separator();
    cout << "Digite a opcion que desea ejecutar" << endl;
    return capturarDato();
}

void procesarOpcionDuendes(int pOpcion) {
    Duende objD1;
    NodoD* nD1 = new NodoD();
    int id, edad, auxID;
    string nombre, beca;
    switch (pOpcion) {
    case 1:
        cout << "1. ListaDuendes - Agregar Duende\n" << endl;

        cout << "Digite Nombre del nuevo Duende\n>>" << endl;
        cin >> nombre;
        cout << "Digite la Edad del nuevo Duende\n>>" << endl;
        edad = capturarDato();
        cout << "Digite el ID del nuevo Duende\n>>" << endl;
        id = capturarDato();

        if (LD->Existe(id)) {
            cout << "Error, Duende Duplicado" << endl;
        }
        else {
<<<<<<< HEAD
            cout << "¿Es el Duende " << nombre << " becado? (Si /No)\n>>" << endl;
=======
            cout << "�Es el Duende " << nombre << " becado? (Si /No)\n>>" << endl;
>>>>>>> David
            cin >> beca;
            objD1.setNombre(nombre); objD1.setEdad(edad); objD1.setID(id);
            if (LD->becadoSiNo(beca)) {
                objD1.setBeca(true);
            }
            else {
                objD1.setBeca(false);
            }
            nD1->setDuende(objD1);
            if (LD->Agregar(nD1)) {
                cout << "Duende Agregado" << endl;
            }
        }
        break;
    case 2:
        cout << "2. ListaDuendes - Eliminar Duende" << endl;
        cout << "\nDigite ID del Duende a Borrar\n>>" << endl;
        auxID = capturarDato();
<<<<<<< HEAD
        if(LM->DuendeEnMatricula(auxID)){
            cout << "\nDuende esta Matriculado, se necesita retirar de la Matricula para eliminarlo\n>>" << endl;
        }else{
            if (LD->Eliminar(auxID)) {
                cout << "\nDuende Borrado\n>>" << endl;
            }
            else {
                cout << "\nError al Borrar\n>>" << endl;
            }
        }
=======
        if (LM->BuscarD(auxID) == NULL) {
            if (LD->Eliminar(auxID)) {
                cout << "Duende eliminado" << endl;
            }
            else {
                cout << "Error al eliminar el duende" << endl;
            }
        }
        else {
            cout << "No se pueden eliminar duendes con Matriculas activas" << endl;
        }
>>>>>>> David
        break;
    case 3:
        cout << "3. ListaDuendes - Editar Duende\n" << endl;
        cout << "Digite ID del Duende a Editar\n>>" << endl;
        id = capturarDato();
        if (LD->Existe(id)) {
            cout << "Digite nuevo Nombre" << endl;
            cin >> nombre;
            cout << "Digite nueva Edad" << endl;
            edad = capturarDato();
<<<<<<< HEAD
            cout << "¿Es el Duende " << nombre << " becado? (Si /No)\n>>" << endl;
=======
            cout << "�Es el Duende " << nombre << " becado? (Si /No)\n>>" << endl;
>>>>>>> David
            cin >> beca;
            objD1.setNombre(nombre); objD1.setEdad(edad); objD1.setID(id);
            if (LD->becadoSiNo(beca)) {
                objD1.setBeca(true);
            }
            else {
                objD1.setBeca(false);
            }
            if (LD->Editar(objD1)) {
                cout << "Duende Actualizado>>" << endl;
            }
        }
        else {
            cout << "Error, Duende No Existe" << endl;
        }
        break;
    case 4:
        cout << "4. ListaDuendes - Buscar Duende\n" << endl;
        cout << "Digite ID del Duende a Buscar\n>>" << endl;
        auxID = capturarDato();
        if (LD->Existe(auxID)) {
            LD->listDuendeSingular((LD->Buscar(auxID)));
        }
        else {
            cout << "Error, Duende No Existe" << endl;
        }
        break;
    case 5:
        cout << "5. ListaDuendes - Listar todos los Duendes\n" << endl;
        LD->ListAll();
        break;
    case 6:
        cout << "6. ListaDuendes - Listar todos los Duendes Becados\n" << endl;
        LD->ListBecados();
        break;
    case 7:
        cout << "7. ListaDuendes - Confirmar si Duende existe en la lista\n" << endl;
        cout << "\nDigite ID del Duende a Buscar\n>>" << endl;
        auxID = capturarDato();
        if (LD->Existe(auxID)) {
            cout << "\nDuende Si Existe\n>>" << endl;
            LD->listDuendeSingular((LD->Buscar(auxID)));
        }
        else {
            cout << "\nDuende No Existe\n>>" << endl;
        }
        break;

    default:
        cout << "Digito una opcion no valida" << endl;
        break;
    }

}

void menuDuendes() {
    int opcion = -1;
    do {
        cout << string(3, '\n');
        cout << "<==============================================>" << endl;
        cout << " =             Escuela Polo Norte             = " << endl;
        cout << " =             Sistema de Duendes             = " << endl;
        cout << "<= = = = = = = = = = = = = = = = = = = = = = ==>" << endl;
        cout << "-  1. Agregar Duende" << endl;
        cout << "-  2. Eliminar Duende" << endl;
        cout << "-  3. Editar Duende" << endl;
        cout << "-  4. Buscar Duende" << endl;
        cout << "-  5. Listar todos los Duendes" << endl;
        cout << "-  6. Listar todos los Duendes Becados" << endl;
        cout << "-  7. Confirmar si Duende existe en la lista" << endl;
        cout << "<==============================================>" << endl;
        cout << "-  0.Salir" << endl;
        opcion = seleccionarOpcion();
        procesarOpcionDuendes(opcion);
    } while (opcion != 0);
}

void procesarOpcionCursos(int pOpcion) {
    int codigo = 0;
    string nombre = " ";
    int creditos = 0;
    int horas = 0;
    Curso curso;
    switch (pOpcion) {
    case 0:
        cout << "Gracias por usar el programa" << endl;
        break;
    case 1:
        cout << "Digite el Codigo (recuerde que debe ser unico)" << endl;
        codigo = capturarDato();
        cout << "Digite el Nombre" << endl;
        cin >> nombre;
        cout << "Digite la cantidad de creditos" << endl;
        creditos = capturarDato();
        cout << "Digite la cantidad de horas" << endl;
        horas = capturarDato();
        cout << "Creando Curso" << endl;
        curso = Curso(codigo, nombre, creditos, horas);
        LC->Agregar(curso);
        cout << "Curso creado" << endl;
        break;
    case 2:
        cout << "Digite el Codigo para eliminar el curso (ESTA ACCION NO PUEDE DESHACERSE)" << endl;
        codigo = capturarDato();
        cout << "Eliminando Curso" << endl;
<<<<<<< HEAD
        if (LC->Eliminar(codigo)) {
            cout << "Curso eliminado" << endl;
        }
        else {
            cout << "Error al eliminar el curso" << endl;
=======
        if (LM->BuscarC(codigo) == NULL) {
            if (LC->Eliminar(codigo)) {
                cout << "Curso eliminado" << endl;
            }
            else {
                cout << "Error al eliminar el curso" << endl;
            }
        }
        else {
            cout << "No se pueden eliminar cursos con Matriculas activas" << endl;
>>>>>>> David
        }
        break;
    case 3:
        cout << "Digite el Codigo del Curso" << endl;
        codigo = capturarDato();
        if (LC->Buscar(codigo) != NULL) {
            cout << "Digite el nuevo Nombre" << endl;
            cin >> nombre;
            cout << "Digite la nueva cantidad de creditos" << endl;
            creditos = capturarDato();
            cout << "Digite la nueva cantidad de horas" << endl;
            horas = capturarDato();
            curso = Curso(codigo, nombre, creditos, horas);
            cout << "Actualizando curso" << endl;
            LC->Editar(curso);
            cout << "Curso actualizado";
        }
        else {
            cout << "No se encontro un curso con el codigo" << endl;
        }
        break;
    case 4:
        cout << "Digite el Codigo del Curso" << endl;
        codigo = capturarDato();
        if (LC->Buscar(codigo) != NULL) {
            curso = LC->Buscar(codigo)->getCurso();
            cout << "Se encontro el curso ->" << " || Curso: Codigo / " << curso.getCodigo() << ", Nombre / " << curso.getNombre() <<
                ", Creditos / " << curso.getCreditos() << ", Horas / " << curso.getHoras() << " ||" << endl;
        }
        else {
            cout << "No se encontro un curso con el codigo" << endl;
        }

        break;
    case 5:
        LC->ListAll();
        break;
    default:
        cout << "Digito una opcion no valida" << endl;
        break;
    }

}

void menuCursos() {
    int opcion = -1;
    do {
        cout << string(3, '\n');
        cout << "<==============================================>" << endl;
        cout << " =              Escuela Polo Norte            = " << endl;
        cout << " =              Sistema de Cursos             = " << endl;
        cout << "<= = = = = = = = = = = = = = = = = = = = = = ==>" << endl;
        cout << "-  1. Agregar un Curso" << endl;
        cout << "-  2. Eliminar un Curso" << endl;
        cout << "-  3. Editar un Curso" << endl;
        cout << "-  4. Buscar un Curso" << endl;
        cout << "-  5. Listar todos los cursos" << endl;
        cout << "<==============================================>" << endl;
        cout << "-  0.Salir" << endl;
        opcion = seleccionarOpcion();
        procesarOpcionCursos(opcion);
    } while (opcion != 0);
}

void procesarOpcionMatriculas(int pOpcion) {
    int id;
    int codigo;
<<<<<<< HEAD
=======
    int nota;
>>>>>>> David
    NodoD* duende;
    NodoC* curso;
    switch (pOpcion) {
    case 0:
        cout << "Gracias por usar el programa" << endl;
        break;
    case 1:
        cout << "Digite el codigo del Curso" << endl;
        codigo = capturarDato();
        cout << "Digite la identificacion del Duende" << endl;
        id = capturarDato();
        duende = LD->Buscar(id);
        curso = LC->Buscar(codigo);
        if (duende != NULL) {
            if (curso != NULL) {
<<<<<<< HEAD
                cout << "Matriculando Duende en Curso" << endl;
                LM->Matricular(duende, curso);
                cout << "Duende Matricualdo en Curso" << endl;
=======
                if (LM->dirDato(id, codigo) == NULL) {
                    cout << "Matriculando Duende en Curso" << endl;
                    LM->Matricular(duende, curso);
                    cout << "Duende Matricualdo en Curso" << endl;
                }
                else {
                    cout << "No se puede registrar el mismo Duende en el Curso" << endl;
                    break;
                }
>>>>>>> David
            }
            else {
                cout << "No se encontro el Curso con ese Codigo" << endl;
                break;
            }
        }
        else {
            cout << "No se encontro el Duende con ese Id" << endl;
            break;
        }        
        break;
    case 2:
        cout << "Digite el codigo del Curso" << endl;
        codigo = capturarDato();
        cout << "Digite la identificacion del Duende" << endl;
        id = capturarDato();
        if (LM->BuscarC(codigo) != NULL && LM->BuscarD(id) != NULL) {
            cout << "Borrando matricula" << endl;
            LM->Retirar(id, codigo);
            cout << "Matricula Borrada" << endl;
        }
        break;
    case 3:
        cout << "Digite la identificacion del Duende" << endl;
        id = capturarDato();
        cout << "Digite el codigo del Curso" << endl;
        codigo = capturarDato();
        cout << "Digite la nueva nota del Duende" << endl;
        nota = capturarDato();
        if (LM->BuscarD(id) != NULL) {
            cout << "Agregando nota" << endl;
            LM->AgregarNota(id, codigo, nota);
            cout << "Nota agregada" << endl;
        }
        else {
            cout << "No existe un Duende con esa identificacion" << endl;
        }
        break;
    case 4:
        LM->ListAll();
        break;

    default:
        cout << "Digito una opcion no valida" << endl;
        break;
    }

}

void menuMatriculas() {
    int opcion = -1;
    do {
        cout << string(3, '\n');
        cout << "<==============================================>" << endl;
        cout << " =              Escuela Polo Norte            =" << endl;
        cout << " =            Sistema de Matriculas           =" << endl;
        cout << "<= = = = = = = = = = = = = = = = = = = = = = ==>" << endl;
        cout << "-  1. Matricular un Duende a un Curso" << endl;
        cout << "-  2. Retirar un Duende de un Curso" << endl;
        cout << "-  3. Agregar nota a un Duende en un Curso" << endl;
        cout << "-  4. Listar todas las Matriculas" << endl;
        cout << "<==============================================>" << endl;
        cout << "-  0.Salir" << endl;
        opcion = seleccionarOpcion();
        procesarOpcionMatriculas(opcion);
    } while (opcion != 0);
}

void procesarOpcionReportes(int pOpcion) {
    switch (pOpcion) {
    case 0:
        cout << "Gracias por usar el programa" << endl;
        break;
    case 1:
        cout << "1. Lista de Cursos - Listar todos los Cursos\n" << endl;
        LC->ListAll();
        break;
    case 2:
        cout << "2. Lista de Duendes - Listar todos los Duendes\n" << endl;
        LD->ListAll();
        break;
    case 3:
        cout << "3. Lista de Duendes - Listar todos los Duendes Becados\n" << endl;
        LD->ListBecados();
        break;
    case 4:
<<<<<<< HEAD
        cout << "3. Lista de Cursos - Listar todos los Cursos con duendes y notas\n" << endl;
        cout << "Opcion en desarrollo" << endl;
=======
        cout << "4. Lista de Matriculas - Listar todos los Cursos con duendes y notas\n" << endl;
        LM->ListAll();
>>>>>>> David
        break;
    default:
        cout << "Digito una opcion no valida" << endl;
        break;
    }

}

void menuReportes() {
    int opcion = -1;
    do {
        cout << string(3, '\n');
        cout << "<==============================================>" << endl;
        cout << " =             Escuela Polo Norte             =" << endl;
        cout << " =             Sistema de Reportes            =" << endl;
        cout << "<= = = = = = = = = = = = = = = = = = = = = = ==>" << endl;
        cout << "-  1. Listar todos los Cursos disponibles" << endl;
        cout << "-  2. Listar todos los Duendes registrados" << endl;
        cout << "-  3. Listar todos los Duendes becados" << endl;
        cout << "-  4. Listar todos los cursos detallados con duendes y notas." << endl;
        cout << "<==============================================>" << endl;
        cout << "-  0.Salir" << endl;
        opcion = seleccionarOpcion();
        procesarOpcionReportes(opcion);
    } while (opcion != 0);
}

void procesarOpcion(int pOpcion) {
    switch (pOpcion) {
    case 0:
        delete LC;
<<<<<<< HEAD
=======
        delete LD;
        delete LM;
>>>>>>> David
        cout << "Gracias por usar el programa" << endl;
        break;
    case 1:
        menuDuendes();
        break;
    case 2:
        menuCursos();
        break;
    case 3:
        menuMatriculas();
        break;
    case 4:
        menuReportes();
        break;

    default:
        cout << "Digito una opcion no valida" << endl;
        break;
    }

}

void mostrarMenu() {
    int opcion = -1;
    do {
        cout << string(3, '\n');
        cout << "<==============================================>" << endl;
        cout << " =              Escuela Polo Norte            =" << endl;
        cout << "<= = = = = = = = = = = = = = = = = = = = = = ==>" << endl;
        cout << "-  1. Menu de Duendes" << endl;
        cout << "-  2. Menu de Cursos" << endl;
        cout << "-  3. Matriculas" << endl;
        cout << "-  4. Reportes" << endl;
        cout << "<==============================================>" << endl;
        cout << "-  0.Salir" << endl;
        opcion = seleccionarOpcion();
        procesarOpcion(opcion);
    } while (opcion != 0);
}


void registrarQuemados() {
    Duende duende;
    NodoD* nodoDuende = new NodoD();
    int cont = 1;
    /*************************************************************/
    duende = Duende(10001, "Mauricio Wright", 42, false);
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(10002, "Bernardo Soto", 32, true);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(10003, "Enrico Palazzo", 55, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(10011, "Maicol Yakson", 25, true);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(10004, "Jarry Poter", 13, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(10007, "Juanix Unix", 25, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(10008, "Chuck Chuldiner", 32, true);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(10010, "Fernando Vicentes", 101, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(10021, "Euronimus Papus", 73, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(10101, "Andrei Messi", 42, true);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(10211, "Tila Piya", 63, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(11001, "Jhammal White", 72, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(10305, "Jared Diens", 47, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(20101, "Pavarano Luccirotti", 55, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(13002, "Lamas de Laine", 32, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(50102, "Walta White", 51, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(19002, "Alessandra Pincetti", 74, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(12021, "Enilda Ramones", 92, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(19071, "Eduarda LaCuarta", 37, false);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    duende = Duende(71005, "Kendy Hong", 52, true);
    nodoDuende = new NodoD();
    nodoDuende->setDuende(duende);
    if (LD->Agregar(nodoDuende)) { cout << "Duende Agregado: " << cont << endl; cont++; }
    /*************************************************************/
    Curso curso;
    cont = 1;

    curso = Curso(101, "Materiales Reciclables para envoltura", 05, 23);
    LC->Agregar(curso);
    cout << "Curso Agregado: " << cont << endl; cont++;
    /*************************************************************/
    curso = Curso(102, "Materiales Sinteticos para envoltura", 03, 20);
    LC->Agregar(curso);
    cout << "Curso Agregado: " << cont << endl; cont++;
    /*************************************************************/
    curso = Curso(103, "Materiales de produccion de Trenes", 03, 20);
    LC->Agregar(curso);
    cout << "Curso Agregado: " << cont << endl; cont++;
    /*************************************************************/
    curso = Curso(201, "Metodos de produccion de marionetas", 06, 27);
    LC->Agregar(curso);
    cout << "Curso Agregado: " << cont << endl; cont++;
    /*************************************************************/
    curso = Curso(202, "Sistemas automatizados de mecanismos de cajas sorpresa", 07, 35);
    LC->Agregar(curso);
    cout << "Curso Agregado: " << cont << endl; cont++;
    /*************************************************************/
    curso = Curso(301, "Logistica y administracion de regalos", 03, 13);
    LC->Agregar(curso);
    cout << "Curso Agregado: " << cont << endl; cont++;
    /*************************************************************/
    curso = Curso(302, "Ofimatica y calendarizacion de asistencia a Santa", 07, 30);
    LC->Agregar(curso);
    cout << "Curso Agregado: " << cont << endl; cont++;
    /*************************************************************/
    curso = Curso(401, "Nutricion y mantenimiento de renos", 04, 14);
    LC->Agregar(curso);
    cout << "Curso Agregado: " << cont << endl; cont++;
    /*************************************************************/
    curso = Curso(402, "Adiestramiento e induccion de nuevos renos (suficiencia)", 06, 30);
    LC->Agregar(curso);
    cout << "Curso Agregado: " << cont << endl; cont++;
    /*************************************************************/
    curso = Curso(501, "Electiva - Duende Superior 'El duende mas feliz'", 02, 10);
    LC->Agregar(curso);
    cout << "Curso Agregado: " << cont << endl; cont++;
    /*************************************************************/
    curso = Curso(502, "Electiva- Duende Internacional 'Lenguas del mundo'", 02, 10);
    LC->Agregar(curso);
    cout << "Curso Agregado: " << cont << endl; cont++;
    /*************************************************************/
}

int main()
{
    cout << "<==============================================>" << endl;
    cout << " =        Bienvenido al sistema de la         =\n";
    cout << " =             Escuela Polo Norte             =\n";
    cout << "<==============================================>" << endl;
    registrarQuemados();
    mostrarMenu();
}

