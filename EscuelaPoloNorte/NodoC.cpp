#include "NodoC.h"

NodoC::NodoC()
{
	this->sgte = NULL;
}

NodoC::NodoC(Curso curso)
{
	this->curso = curso;
	this->sgte = NULL;
}

void NodoC::setCurso(Curso c)
{
	this->curso = c;
}

void NodoC::setSgte(NodoC* sgte)
{
	this->sgte = sgte;
}

Curso NodoC::getCurso()
{
	return this->curso;
}

NodoC* NodoC::getSgte()
{
	return this->sgte;
}
