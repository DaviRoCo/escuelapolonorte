#pragma once
#include "Curso.h"
class NodoC
{
private:
	Curso curso;
	NodoC* sgte;

public:
	NodoC();
	NodoC(Curso);

	void setCurso(Curso);
	void setSgte(NodoC*);

	Curso getCurso();
	NodoC* getSgte();
};

