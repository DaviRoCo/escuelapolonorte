#include "NodoD.h"

NodoD::NodoD()
{
	this->ante = NULL;
	this->sgte = NULL;
}

NodoD::NodoD(Duende d)
{
	this->duende = d;
	this->ante = NULL;
	this->sgte = NULL;
}

void NodoD::setDuende(Duende d)
{
	this->duende = d;
}

void NodoD::setSgte(NodoD* sgte)
{
	this->sgte = sgte;
}

void NodoD::setAnte(NodoD* ante)
{
	this->ante = ante;
}

Duende NodoD::getDuende()
{
	return this->duende;
}

NodoD* NodoD::getSgte()
{
	return this->sgte;
}

NodoD* NodoD::getAnte()
{
	return this->ante;
}

bool NodoD::esBecado(int)
{
	return false;
}
