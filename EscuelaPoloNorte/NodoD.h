#pragma once
#include <iostream>
#include <stdlib.h>
#include "Duende.h"
using namespace std;

class NodoD
{
private:
	Duende duende;
	NodoD* sgte;
	NodoD* ante;

public:
	NodoD();
	NodoD(Duende);

	void setDuende(Duende);
	void setSgte(NodoD*);
	void setAnte(NodoD*);

	Duende getDuende();
	NodoD* getSgte();
	NodoD* getAnte();

	bool esBecado(int);
};

