#include "NodoM.h"

NodoM::NodoM()
{
	this->sgte = NULL;
	this->ante = NULL;
}

NodoM::NodoM(Matricula* matricula)
{
	this->matricula = matricula;
	this->sgte = NULL;
	this->ante = NULL;
}

Matricula* NodoM::getMatricula()
{
	return this->matricula;
}

NodoM* NodoM::getSgte()
{
	return this->sgte;
}

NodoM* NodoM::getAnte()
{
	return this->ante;
}

void NodoM::setMatricula(Matricula*)
{
	this->matricula = matricula;
}

void NodoM::setSgte(NodoM* sgte)
{
	this->sgte = sgte;
}

void NodoM::setAnte(NodoM* ante)
{
	this->ante = ante;
}
