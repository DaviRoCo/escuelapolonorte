#pragma once
#include "Matricula.h"

class NodoM
{
private:
	Matricula* matricula;
	NodoM* sgte;
	NodoM* ante;
public:
	NodoM();
	NodoM(Matricula*);

	Matricula* getMatricula();
	NodoM* getSgte();
	NodoM* getAnte();

	void setMatricula(Matricula*);
	void setSgte(NodoM*);
	void setAnte(NodoM*);
};

