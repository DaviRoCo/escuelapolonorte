
#include "listaCursos.h"
#include <iostream>

listaCursos::listaCursos()
{
	this->cab = NULL;
	this->largo = 0;
}

NodoC* listaCursos::getCab()
{
	return this->cab;
}

int listaCursos::getLargo()
{
	return this->largo;
}

void listaCursos::setCab(NodoC* cab)
{
	this->cab = cab;
}

void listaCursos::setLargo(int l)
{
	this->largo = l;
}

void listaCursos::Agregar(Curso curso)
{
	if (Buscar(curso.getCodigo()) == NULL) {
		bool agregado = false;
		NodoC* aux;
		NodoC* ant;
		NodoC* temp = new NodoC(curso);
		if (esVacia())
		{
			agregarInicio(curso);
			agregado = true;
		}
		else {
			if (!Existe(curso.getCodigo()))
			{
				aux = getCab();
				ant = getCab();
				while (!agregado)
				{
					if (aux->getCurso().getCodigo() < curso.getCodigo())
					{
						if (aux->getSgte() != NULL)
						{
							ant = aux;
							aux = ant->getSgte();
						}
						else
						{
							agregarFinal(curso);
							agregado = true;
						}
					}
					if (aux->getCurso().getCodigo() > curso.getCodigo())
					{
						if (aux == getCab())
						{
							temp->setSgte(aux);
							setCab(temp);
							setLargo(getLargo() + 1);
							agregado = true;
						}
						else
						{
							temp->setSgte(aux);
							ant->setSgte(temp);
							agregado = true;
						}
					}
				}
			}
		}
	}
}

bool listaCursos::Eliminar(int codigo)
{
	bool eliminado = false;
	NodoC* aux = NULL;
	if (!esVacia()) {
		if (dirDato(codigo) != NULL)
		{
			if (getCab()->getCurso().getCodigo() == codigo) {
				aux = getCab();
				setCab(aux->getSgte());
			}
			else {
				NodoC* ant = dirAnterior(codigo);
				if (ant != NULL) {
					aux = ant->getSgte();
					ant->setSgte(aux->getSgte());
				}

			}
			if (aux != NULL) {
				delete aux;
				setLargo(getLargo() - 1);
				eliminado = true;
			}
		} 
	}
	return  eliminado;
}


bool listaCursos::Editar(Curso cursoActualizado)
{
	NodoC* aux = Buscar(cursoActualizado.getCodigo());
	if (aux != NULL) {
		aux->setCurso(cursoActualizado);
		return true;
	}
	else {
		return false;
	}

}

NodoC* listaCursos::Buscar(int codigo)
{
	return dirDato(codigo);
}

void listaCursos::ListAll()
{
	if (!esVacia()) {
		cout << endl;
		NodoC* aux = getCab();
		cout << "DESPLEGAR LA LISTA \n";
		while (aux != NULL) {
<<<<<<< HEAD
			cout << "->" << " || Curso: Codigo / " << aux->getCurso().getCodigo() << 
				", Nombre / " << aux->getCurso().getNombre() << ", Creditos / " << 
				aux->getCurso().getCreditos() << ", Horas / " << 
				aux->getCurso().getHoras() << " ||";
=======
			cout << "-------------------------" << endl;
			cout << "-------------------------" << endl;
			cout << "CURSO:" << endl;
			cout << "Codigo: " << aux->getCurso().getCodigo() << endl;
			cout << "Curso: " << aux->getCurso().getNombre() << endl;
			cout << "Creditos: " << aux->getCurso().getCreditos() << endl;
			cout << "Horas: " << aux->getCurso().getHoras() << endl;
>>>>>>> David
			cout << endl;
			aux = aux->getSgte();
		}
		cout << "Final \n\n";
	}
}

bool listaCursos::Existe(int codigo)
{
	return dirDato(codigo) != NULL;
}

bool listaCursos::esVacia()
{
	return this->cab == NULL;
}
<<<<<<< HEAD

=======
>>>>>>> David
NodoC* listaCursos::dirDato(int codigo) {
	NodoC* dir = NULL;
	NodoC* aux = getCab();
	while (aux != NULL && dir == NULL) {
		if (aux->getCurso().getCodigo() == codigo) {
			dir = aux;
		}
		else {
			aux = aux->getSgte();
		}
	}
	return dir;
}
NodoC* listaCursos::dirAnterior(int codigo)
{

	NodoC* ant = NULL;
	NodoC* aux = getCab();
	while (aux != NULL && aux->getSgte() != NULL && ant == NULL) {
		if (aux->getSgte()->getCurso().getCodigo() == codigo) {
			ant = aux;
		}
		else {
			aux = aux->getSgte();
		}
	}

	return ant;
<<<<<<< HEAD
}

NodoC* listaCursos::dirUltimo() {
	NodoC* aux = getCab();
	if (!esVacia())
	{
		while (aux->getSgte() != NULL)
		{
			aux = aux->getSgte();
		}
	}
	return aux;
}
void listaCursos::agregarInicio(Curso curso) {
	NodoC* temp = new NodoC(curso);
	temp->setSgte(getCab());
	setCab(temp);
	setLargo(getLargo() + 1);
}

=======
}

NodoC* listaCursos::dirUltimo() {
	NodoC* aux = getCab();
	if (!esVacia())
	{
		while (aux->getSgte() != NULL)
		{
			aux = aux->getSgte();
		}
	}
	return aux;
}
void listaCursos::agregarInicio(Curso curso) {
	NodoC* temp = new NodoC(curso);
	temp->setSgte(getCab());
	setCab(temp);
	setLargo(getLargo() + 1);
}

>>>>>>> David
void listaCursos::agregarFinal(Curso curso) {
	NodoC* temp = new NodoC(curso);
	if (esVacia())
	{
		agregarInicio(curso);
	}
	else {
		NodoC* aux = dirUltimo();
		aux->setSgte(temp);
		setLargo(getLargo() + 1);
	}
}