#pragma once
#include "NodoC.h"
class listaCursos
{
private:
	NodoC* cab;
	int largo;
public:
	listaCursos();

	NodoC* getCab();
	int getLargo();

	void setCab(NodoC*);
	void setLargo(int);

	void Agregar(Curso);
	bool Eliminar(int);
	bool Editar(Curso);
	NodoC* Buscar(int);
	void ListAll();
	bool Existe(int);
	bool esVacia();
	NodoC* dirDato(int);
	NodoC* dirAnterior(int);
	NodoC* dirUltimo();
	void agregarInicio(Curso);
	void agregarFinal(Curso);
};

