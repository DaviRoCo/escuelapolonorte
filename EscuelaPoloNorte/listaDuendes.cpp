#include "listaDuendes.h"
#include<iostream>
#include <algorithm>
#include <stdlib.h>

listaDuendes::listaDuendes()
{
    this->cab = NULL;
    this->largo = 0;
}

NodoD* listaDuendes::getCab()
{
    return this->cab;
}

int listaDuendes::getLargo()
{
    return this->largo;
}

void listaDuendes::setCab(NodoD* cab)
{
    this->cab = cab;
}

void listaDuendes::setLargo(int l)
{
    this->largo = l;
}
void listaDuendes::AgregarInicio(NodoD* newDuende) {
    if (!esVacia()) {
        newDuende->setSgte(getCab());
        getCab()->setAnte(newDuende);
    }
    setCab(newDuende);
    setUlt(newDuende);
    setLargo(getLargo() + 1);
}
bool listaDuendes::Agregar(NodoD* newNode)
{
    bool b = false;
    if (esVacia() || (newNode->getDuende().getID()) < (getCab()->getDuende().getID())) {
        AgregarInicio(newNode);
        b = true;
    }
    else {
        NodoD* pointer = getCab();
        while (pointer != nullptr) {
            if (newNode->getDuende().getID() == pointer->getDuende().getID()) {
                break;
            }
            else if (pointer->getSgte() == nullptr) {
                newNode->setSgte(nullptr);
                newNode->setAnte(pointer);
                pointer->setSgte(newNode);
                setUlt(newNode);
                b = true;
                setLargo(getLargo() + 1);
                break;
            }
            else if ((newNode->getDuende().getID() > pointer->getDuende().getID()) && (newNode->getDuende().getID() < pointer->getSgte()->getDuende().getID())) {
                newNode->setSgte(pointer->getSgte());
                newNode->setAnte(pointer);
                pointer->getSgte()->setAnte(newNode);
                pointer->setSgte(newNode);
                b = true;
                setLargo(getLargo() + 1);
                break;
            }
            else {
                pointer = pointer->getSgte();
            }
        }
    }
    return b;
}

bool listaDuendes::Eliminar(int pDato) {
    NodoD* pointer = getCab(); //se�alar el nodo que va a ser eliminado
    bool eliminado = false;
    if (getCab()->getDuende().getID() == pDato) {
        setCab(pointer->getSgte());
        delete (pointer);
        setLargo(getLargo() - 1);
        eliminado = true;
    }
    else { //Sirve para eliminar en medio y el �ltimo
        while (pointer != nullptr) {
            if (pointer->getDuende().getID() == pDato) {
                if (pointer->getSgte() == nullptr) {
                    setUlt(pointer->getAnte());
                    pointer->getAnte()->setSgte(nullptr);
                    delete pointer;
                    setLargo(getLargo() - 1);
                    eliminado = true;
                    break;
                }
                else {
                    (pointer->getAnte())->setSgte(pointer->getSgte());
                    (pointer->getSgte())->setAnte(pointer->getAnte());
                    delete pointer;
                    setLargo(getLargo() - 1);
                    eliminado = true;
                    break;
                }
            }
            else {
                pointer = pointer->getSgte();
            }
        }
    }
    return eliminado;
}

bool listaDuendes::becadoSiNo(string st) {
    bool b = false;
    transform(st.begin(), st.end(), st.begin(), ::tolower);
    if ((st.compare("si") == 0) || (st.compare("s") == 0)) {
        b = true;
    }
    return b;
}

bool listaDuendes::Editar(Duende pd) {
    NodoD* nodoDuende = Buscar(pd.getID());
    bool b;
    if (nodoDuende != nullptr) {
        nodoDuende->setDuende(pd);
        b = true;
    }
    else {
        b = false;
    }
    return b;
}

NodoD* listaDuendes::Buscar(int pID) {
    NodoD* match = NULL;
    NodoD* pd = getCab();
    while (pd != nullptr) {
        if (pID == pd->getDuende().getID()) {
            match = pd;
            break;
        }
        else {
            pd = pd->getSgte();
        }
    }
    return match;
}



void listaDuendes::ListAll() {
    NodoD* pD = getCab();
    while (pD != nullptr) {
        cout << "-------------------------" << endl;
        cout << "-------------------------" << endl;
        cout << "DUENDE:" << endl;
        cout << "Nombre: " << pD->getDuende().getNombre() << endl;
        cout << "Edad: " << pD->getDuende().getEdad() << endl;
        cout << "ID: " << pD->getDuende().getID() << endl;
        if (pD->getDuende().getBeca() == true) {
            cout << "Becado: Si" << endl;
        }
        else {
            cout << "Becado: No" << endl;
        }
        cout << endl;
        pD = pD->getSgte();
    }
    cout << "Final \n\n";
}

void listaDuendes::ListBecados() {
    NodoD* pD = getCab();
    while (pD != nullptr) {
        if (pD->getDuende().getBeca() == true) {
            cout << "-------------------------" << endl;
            cout << "-------------------------" << endl;
            cout << "DUENDE:" << endl;
            cout << "Nombre: " << pD->getDuende().getNombre() << endl;
            cout << "Edad: " << pD->getDuende().getEdad() << endl;
            cout << "ID: " << pD->getDuende().getID() << endl;
            if (pD->getDuende().getBeca() == true) {
                cout << "Becado: Si" << endl;
            }
            else {
                cout << "Becado: No" << endl;
            }
            cout << endl;
        }
        pD = pD->getSgte();
    }
    cout << "Final \n\n";
}

void listaDuendes::listDuendeSingular(NodoD* pD) {
    cout << "\n### Informacion del Duende: ###\n" << endl;
    cout << "DUENDE:" << endl;
    cout << "Nombre: " << pD->getDuende().getNombre() << endl;
    cout << "Edad: " << pD->getDuende().getEdad() << endl;
    cout << "ID: " << pD->getDuende().getID() << endl;
    if (pD->getDuende().getBeca() == true) {
        cout << "Becado: Si" << endl;
    }
    else {
        cout << "Becado: No" << endl;
    }
    cout << "Final \n\n";
    cout << endl;
}

bool listaDuendes::Existe(int pId) {
    bool ex = false;
    NodoD* pD = getCab();
    while (pD != nullptr) {
        Duende objD = pD->getDuende();
        if (objD.getID() == pId) {
            ex = true;
            break;
        }
        pD = pD->getSgte();
    }
    return ex;
}

bool listaDuendes::esVacia() {
    bool v = false;
    if (getCab() == nullptr) {
        v = true;
    }
    return v;
}

NodoD* listaDuendes::getUlt() {
    return ult;
}

void listaDuendes::setUlt(NodoD* ult) {
    listaDuendes::ult = ult;
}
