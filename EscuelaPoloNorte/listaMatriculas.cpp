#include "listaMatriculas.h"

listaMatriculas::listaMatriculas()
{
    this->cab = NULL;
    this->largo = 0;
}

NodoM* listaMatriculas::getCab()
{
    return this->cab;
}

int listaMatriculas::getLargo()
{
    return this->largo;
}

void listaMatriculas::setCab(NodoM* cab)
{
    this->cab = cab;
}

void listaMatriculas::setLargo(int l)
{
    this->largo = l;
}

void listaMatriculas::AgregarNota(int id, int codigo, int nota)
{
    NodoM* aux = dirDato(id, codigo);
    aux->getMatricula()->setNota(nota);
}

<<<<<<< HEAD
void listaMatriculas::Matricular(NodoD* duende, NodoC* curso)
{
    NodoM* nuevo = new NodoM(new Matricula(curso->getCurso().getCodigo(), duende->getDuende().getID(), 0, curso, duende));
    nuevo->setSgte(getCab());
    setCab(nuevo);
    setLargo(getLargo() + 1);
}

NodoM* listaMatriculas::Retirar(int, int)
{
    return nullptr;
}

NodoC* listaMatriculas::BuscarC(int)
{
    return nullptr;
}

NodoD* listaMatriculas::BuscarD(int idDuende){
    NodoM *pointer = getCab();
    NodoD *nodoDuende;
    while(pointer != nullptr){
        if(pointer->getMatricula()->getDuendePtr()->getDuende().getID() == idDuende){
            nodoDuende = pointer->getMatricula()->getDuendePtr();
        }
        pointer = pointer->getSgte();
    }
    return nodoDuende;
}

bool listaMatriculas::DuendeEnMatricula(int idDuende){
    NodoM *pointer = getCab();
    bool b = false;
    while(pointer != nullptr){
        if(pointer->getMatricula()->getDuendePtr()->getDuende().getID() == idDuende){
            b = true;
            break;
        }
        pointer = pointer->getSgte();
    }
    return b;
=======
void listaMatriculas::MatricularInicio(NodoM* newNodo)
{
    if (!esVacia()) {
        newNodo->setSgte(getCab());
        getCab()->setAnte(newNodo);
    }
    setCab(newNodo);
    setLargo(getLargo() + 1);
}

void listaMatriculas::Matricular(NodoD* duende, NodoC* curso)
{
    bool b = false;
    NodoM* nuevo = new NodoM(new Matricula(curso->getCurso().getCodigo(), duende->getDuende().getID(), 0, curso, duende));
    if (esVacia() || (nuevo->getMatricula()->getCodigoCurso()) < (getCab()->getMatricula()->getCodigoCurso())) {
        MatricularInicio(nuevo);
        b = true;
    }
    else {
        NodoM* pointer = getCab();
        for (int i = 0; i < getLargo(); i++) {
            if (nuevo->getMatricula()->getCodigoCurso() == pointer->getMatricula()->getCodigoCurso()) {
                break;
            }
            else if (pointer->getSgte() == nullptr) {
                nuevo->setSgte(nullptr);
                nuevo->setAnte(pointer);
                pointer->setSgte(nuevo);
                b = true;
                setLargo(getLargo() + 1);
                break;
            }
            else if ((nuevo->getMatricula()->getCodigoCurso() > pointer->getMatricula()->getCodigoCurso()) && (nuevo->getMatricula()->getCodigoCurso() < pointer->getSgte()->getMatricula()->getCodigoCurso())) {
                nuevo->setSgte(pointer->getSgte());
                nuevo->setAnte(pointer);
                pointer->getSgte()->setAnte(nuevo);
                pointer->setSgte(nuevo);
                b = true;
                setLargo(getLargo() + 1);
                break;
            }
            else {
                pointer = pointer->getSgte();
            }
        }
    }
}

NodoM* listaMatriculas::Retirar(int id, int codigo)
{
    bool eliminado = false;
    NodoM* aux = NULL;
    aux = dirDato(id, codigo);
    if (aux != NULL) {
        if (aux == getCab()) {
            setCab(aux->getSgte());
        }
        else {
            aux->getAnte()->setSgte(aux->getSgte());
        }
        if (aux->getSgte() != NULL) {
            aux->getSgte()->setAnte(aux->getAnte());
        }

        delete aux; //eliminar el nodo.
        setLargo(getLargo() - 1);
    }
    return aux;
}

NodoC* listaMatriculas::BuscarC(int codigo)
{
    NodoC* match = NULL;
    NodoM* pd = getCab();
    for (int i = 0; i < getLargo(); i++) {
        if (codigo == pd->getMatricula()->getCodigoCurso()) {
            match = pd->getMatricula()->getCursoPtr();
            break;
        }
    }
    return match;
}

NodoD* listaMatriculas::BuscarD(int id)
{
    NodoD* match = NULL;
    NodoM* pd = getCab();
    for (int i = 0; i < getLargo(); i++) {
        if (id == pd->getMatricula()->getDuendeID()) {
            match = pd->getMatricula()->getDuendePtr();
            break;
        }
    }
    return match;
>>>>>>> David
}

void listaMatriculas::ListAll()
{
    if (!esVacia()) {
        NodoM* pM = getCab();
        int l = getLargo();
        for (int i = 0; i < l; i++) {
<<<<<<< HEAD
=======
            cout << "-------------------------" << endl;
            cout << "-------------------------" << endl;
            cout << "DUENDE:" << endl;
>>>>>>> David
            cout << "ID: " << pM->getMatricula()->getDuendePtr()->getDuende().getID() << endl;
            cout << "Nombre: " << pM->getMatricula()->getDuendePtr()->getDuende().getNombre() << endl;
            cout << "Edad: " << pM->getMatricula()->getDuendePtr()->getDuende().getEdad() << endl;
            if (pM->getMatricula()->getDuendePtr()->getDuende().getBeca() == true) {
                cout << "Becado: Si" << endl;
            }
            else {
                cout << "Becado: No" << endl;
            }
            cout << "-------------------------" << endl;
<<<<<<< HEAD
=======
            cout << "-------------------------" << endl;
            cout << "CURSO:" << endl;
>>>>>>> David
            cout << "Codigo: " << pM->getMatricula()->getCursoPtr()->getCurso().getCodigo() << endl;
            cout << "Curso: " << pM->getMatricula()->getCursoPtr()->getCurso().getNombre() << endl;
            cout << "Creditos: " << pM->getMatricula()->getCursoPtr()->getCurso().getCreditos() << endl;
            cout << "Horas: " << pM->getMatricula()->getCursoPtr()->getCurso().getHoras() << endl;
            cout << "-------------------------" << endl;
            cout << "Nota del Duende: " << pM->getMatricula()->getNota() << endl;
            pM = pM->getSgte();
        }
<<<<<<< HEAD
    }
}

bool listaMatriculas::esVacia()
{
    bool v = false;
    if (getCab() == nullptr) {
        v = true;
    }
    return v;
=======
        cout << "Final \n\n";
    }
>>>>>>> David
}

bool listaMatriculas::esVacia()
{
    bool v = false;
    if (getCab() == nullptr) {
        v = true;
    }
    return v;
}
NodoM* listaMatriculas::dirDato(int id, int codigo)
{
    NodoM* dir = NULL;
    NodoM* aux = getCab();
    int cont = getLargo();
    for (int i = 0; i < cont; i++)
    {
        if (aux->getMatricula()->getCodigoCurso() == codigo && aux->getMatricula()->getDuendeID() == id) {
            dir = aux;
        }
        else {
            aux = aux->getSgte();
        }
    }
    return dir;
}